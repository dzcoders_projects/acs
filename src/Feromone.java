import java.util.Vector;

/**
 * Created by chayma on 4/27/17.
 */
public class Feromone {
    private Vector<Float> literalPositif;
    private Vector<Float> literlNegatif;
    private static float tauxVaporisation;
    private static float feromoneInitial;
    private Ant ant;

    public Feromone(Ant ant) {
        this.ant = ant;
        initializeVectors();
    }

    public Feromone() {
        tauxVaporisation = Ant.getAcs().getRo();
        feromoneInitial = Ant.getAcs().getTo();
        initializeVectors();
    }

    public Feromone(Ant ant, Feromone feromone){
        this.ant= ant;
        literalPositif= (Vector<Float>) feromone.literalPositif.clone();
        literlNegatif= (Vector<Float>) feromone.literlNegatif.clone();

    }

    private void initializeVectors(){
        int taille= Ant.getAcs().getChamps().getNombreVariable();
        literalPositif= new Vector<>();
        literlNegatif= new Vector<>();
        for (int i=0; i< taille; i++){
            literalPositif.add(i, feromoneInitial);
            literlNegatif.add(i, feromoneInitial);
        }
    }

    public void onlineUpdate(){
         float newValue;
        for (Integer litteral :ant.getInterpretation()){
                newValue= (1- tauxVaporisation) * getFeromoneOf(litteral)+ tauxVaporisation* feromoneInitial;
                setFeromoneOf(litteral, newValue);
        }
    }

    public void offlineUpfdate(Feromone feromone){
        int nbVariables= Ant.getAcs().getChamps().getNombreVariable();
        float newValue;
        for (Integer litteral :ant.getInterpretation()){
                newValue= (1- tauxVaporisation)* getFeromoneOf(litteral)+
                        tauxVaporisation* (feromone.getFeromoneOf(litteral)-getFeromoneOf(litteral));
                setFeromoneOf(litteral, newValue);
        }
    }

    public float getFeromoneOf(int i){
        if (i>0){
            i-=1;
            return literalPositif.get(i);
        }
        else{
            i = -i-1;
            return literlNegatif.get(i);
        }
    }

    public Vector<Float> getLiteralPositif() {
        return literalPositif;
    }

    public void setLiteralPositif(Vector<Float> literalPositif) {
        this.literalPositif = literalPositif;
    }

    public Vector<Float> getLiterlNegatif() {
        return literlNegatif;
    }

    public void setLiterlNegatif(Vector<Float> literlNegatif) {
        this.literlNegatif = literlNegatif;
    }

    public static float getTauxVaporisation() {
        return tauxVaporisation;
    }

    public static void setTauxVaporisation(float tauxVaporisation) {
        Feromone.tauxVaporisation = tauxVaporisation;
    }

    public static float getFeromoneInitial() {
        return feromoneInitial;
    }

    public static void setFeromoneInitial(float feromoneInitial) {
        Feromone.feromoneInitial = feromoneInitial;
    }

    public Ant getAnt() {
        return ant;
    }

    public void setAnt(Ant ant) {
        this.ant = ant;
    }

    public void setFeromoneOf(int i, float f){
        if (i>0){
            i-=1;
             literalPositif.set(i,f);
        }
        else{
            i = -i-1;
            literlNegatif.set(i,f);
        }
    }

    @Override
    public String toString() {
        return "Feromone{" +
                "literalPositif=" + literalPositif +
                ", literlNegatif=" + literlNegatif +
                '}';
    }
    public void checkFeromone(){
        for (int i=0;i<literlNegatif.size();i++){
            if (literlNegatif.get(i)<0 || literalPositif.get(i)<0){
                System.out.println("erreur feromone negatif");
            }
        }
    }
}
