import com.sun.xml.internal.ws.config.metro.dev.FeatureReader;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by chayma on 4/27/17.
 */
public class ACS {
    private int maxItereration,nbAnt, maxSteps;
    private Ant globalBest;
    private float to, ro,alpha,beta,q0;
    private  CNF champs;

    public ACS(int maxItereration, int nbAnt, float to, float ro, float alpha, float beta, float q0, int maxSteps, CNF champs) {
        this.maxItereration = maxItereration;
        this.nbAnt = nbAnt;
        this.to = to;
        this.ro = ro;
        this.alpha = alpha;
        this.beta = beta;
        this.q0 = q0;
        this.champs = champs;
        this.maxSteps= maxSteps;
        Feromone.setFeromoneInitial(to);
        Feromone.setTauxVaporisation(ro);
        Ant.setAcs(this);
    }

    public Ant demarer(){
        int nbClauses=Ant.getAcs().getChamps().getNombreClauses();
        globalBest = new Ant();
        globalBest.buildSolution();
        globalBest.getFeromone().onlineUpdate();
        globalBest.getFeromone().checkFeromone();
        globalBest.evaluer();
        for (int i=0;i<maxItereration;i++){
            if(globalBest.getFitness() == nbClauses){
                return globalBest;
            }
            HashSet<Ant> antSet = new HashSet<>();
            for (int j=0;j<nbAnt;j++){
                Ant ant = new Ant(globalBest);
                ant.buildSolution();
                ant.evaluer();
                ant.improveSearch(maxSteps);
                ant.getFeromone().onlineUpdate();
                ant.getFeromone().checkFeromone();
                antSet.add(ant);
            }
            Ant bestIterarion = Collections.max(antSet);
            if (globalBest.compareTo(bestIterarion)<0){
                globalBest.getFeromone().offlineUpfdate(bestIterarion.getFeromone());
                globalBest.setInterpretation(bestIterarion.getInterpretation());
                globalBest.getFeromone().checkFeromone();
                globalBest.setFitness(bestIterarion.getFitness());
            }
        }
        return globalBest;
    }

    public float getBeta() {
        return beta;
    }

    public float getAlpha() {
        return alpha;
    }

    public float getQ0() {
        return q0;
    }

    public void setQ0(float q0) {
        this.q0 = q0;
    }

    public int getMaxItereration() {
        return maxItereration;
    }

    public void setMaxItereration(int maxItereration) {
        this.maxItereration = maxItereration;
    }

    public Ant getGlobalBest() {
        return globalBest;
    }

    public void setGlobalBest(Ant globalBest) {
        this.globalBest = globalBest;
    }

    public float getTo() {
        return to;
    }

    public void setTo(float to) {
        this.to = to;
    }

    public float getRo() {
        return ro;
    }

    public void setRo(float ro) {
        this.ro = ro;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public void setBeta(float beta) {
        this.beta = beta;
    }

    public CNF getChamps() {
        return champs;
    }

    public void setChamps(CNF champs) {
        this.champs = champs;
    }

    @Override
    public String toString() {
        return "ACS{" +
                "maxItereration=" + maxItereration +
                ", nbAnt=" + nbAnt +
                ", to=" + to +
                ", ro=" + ro +
                ", alpha=" + alpha +
                ", beta=" + beta +
                ", q0=" + q0 +
                '}';
    }
}
