import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

/**
 * Created by bot on 4/30/17.
 */
public class Table {
    private List<Float> literalPositif;
    private List<Float> literalNegatif;

    public Table(int nbvariable ,Float init) {
        initializeVectors(nbvariable, init);
    }
    private void initializeVectors(int taille,Float init){
        literalPositif= new Vector<>();
        literalNegatif = new Vector<>();
        for (int i=0; i< taille; i++){
            literalPositif.add(i, init);
            literalNegatif.add(i, init);
        }
    }
    public float getElementOf(int i){
        if (i>0){
            i-=1;
            return literalPositif.get(i);
        }
        else{
            i = -i-1;
            return literalNegatif.get(i);
        }
    }
    public void  setElementOf(int i,float element){
        if (i>0){
            i-=1;
            literalPositif.set(i,element);
        }
        else{
            i = -i-1;
            literalNegatif.set(i,element);
        }
    }
    @Override
    public String toString() {
        return "Table{" +
                "literalPositif=" + literalPositif +
                ", literalNegatif=" + literalNegatif +
                '}';
    }
}
