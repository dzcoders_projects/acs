import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by chayma on 4/27/17.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        long deadline=15*60*1000;
        int maxIteration=400,
                nbAnt=1,
                maxSteps= 100;
        float to=0.1f,
                ro= 0.2057f,
                alpha= 0.3f,
                beta= 0.8f,
                q0= 0.01f;
        System.out.println("maxIter="+maxIteration + "  nbAnt=" + nbAnt+ "  maxSteps=" + maxSteps + "  to=" +
                to+ "  ro=" + ro+ "  alpha=" + alpha+ "  beta=" + beta+ "  q0=" + q0);
        for (int i=1;i<=10;i++){
            CNF cnf =new CNF("files/uf75-0"+i+".cnf");
            ACS acs= new ACS(maxIteration,nbAnt,to,ro,alpha,beta,q0,maxSteps,cnf);
            double debut = System.currentTimeMillis();
            Ant ant= acs.demarer();
            double temps = (System.currentTimeMillis() - debut) * 0.001;
            FileWriter fileWriter = new FileWriter(maxIteration + "_" + nbAnt+ "_" + maxSteps + "_" +
                    to+ "_" + ro+ "_" + alpha+ "_" + beta+ "_" + q0, true);
            fileWriter.write("uf75-0" + i + ".cnf\t" + ant.getFitness() + "\t" + temps + "\n");
            fileWriter.close();
            System.out.println("uf75-0" + i + ".cnf\t" + ant.getFitness() + "\t" + temps + " s");

        }
    }

}
