/**
 * Created by bot on 5/1/17.
 */
public class Couple  implements Comparable<Couple> {
    int litteral;
    float prob;

    public Couple(int litteral) {
        this.litteral = litteral;
        prob = 0;
    }

    public int getLitteral() {
        return litteral;
    }

    public void setLitteral(int litteral) {
        this.litteral = litteral;
    }

    public float getProb() {
        return prob;
    }

    public void setProb(float prob) {
        this.prob = prob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Couple couple = (Couple) o;

        return litteral == couple.litteral;
    }

    @Override
    public int compareTo(Couple o) {
        return prob > o.prob ? 1 : prob == o.prob ? 0: -1;
    }

    @Override
    public String toString() {
        return "Couple{" +
                "litteral=" + litteral +
                ", prob=" + prob +
                '}';
    }

    @Override
    public int hashCode() {
        return litteral;
    }
}
