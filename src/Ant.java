import java.lang.reflect.Array;
import java.util.*;
/**
 * Created by chayma on 4/27/17.
 */
public class Ant implements Comparable<Ant>{

    private HashSet<Integer> interpretation;
    private static ACS acs;
    private int fitness= -1;
    private Feromone feromone;

    public Ant() {
        interpretation = new HashSet<>();
        feromone = new Feromone(this);
    }

    public Ant(Ant ant) {
        interpretation = new HashSet<>();
        this.feromone = new Feromone(this,ant.getFeromone());
    }

    public Ant(HashSet<Integer> interpretation) {
        this.interpretation = new HashSet<>(interpretation);
    }

    public void improveSearch(int maxSteps){

        if(fitness== Ant.getAcs().getChamps().getNombreClauses()) return;
        for(int i=0; i< maxSteps; i++){
            Ant currentAnt= new Ant(interpretation);
            ArrayList<Clause> clausesUnsat= new ArrayList<>(currentAnt.getUnsatisfiedClauses());

            //select une clause non satisfiable aleatoirement
            Clause clause= clausesUnsat.get(new Random().nextInt(clausesUnsat.size()));

            //select un litteral aleatoirement
            ArrayList<Integer>litteraux= new ArrayList<>(clause.getLitteraux());
            int litteral= litteraux.get(new Random().nextInt(litteraux.size()));

            //inverser ce litteral
            currentAnt.interpretation.remove(-litteral);
            currentAnt.interpretation.add(litteral);
            currentAnt.evaluer();

            if(currentAnt.getFitness()> fitness){
                //affecter l'interpretation
                interpretation= new HashSet<>(currentAnt.interpretation);
                //affecter fitness
                fitness= currentAnt.fitness;
                if(fitness == acs.getChamps().getNombreClauses()) break;
            }
        }

    }

    public HashSet<Clause> getUnsatisfiedClauses(){

        HashSet<Clause> unsatisfiedClause= new HashSet<>();
        HashSet<Clause> clauses= acs.getChamps().getClauses();
        for (Clause clause: clauses){
            boolean contient= false;
            for (int variable: clause.getLitteraux()){
                if(interpretation.contains(variable)){
                    contient=true;
                    break;
                }
            }
            if(!contient){
                unsatisfiedClause.add(new Clause(clause));
            }
        }
        return unsatisfiedClause;
    }

    public void evaluer(){
        HashSet<Clause> clauses= acs.getChamps().getClauses();
        int fitness=0;
        for (Clause clause: clauses){
            for (int variable: clause.getLitteraux()){
                if(interpretation.contains(variable)){
                        fitness++;
                        break;
                }
            }
        }
        this.fitness = fitness;
    }


    public void buildSolution(){
        HashSet<Couple> l = ensembleInitial(); // ensemble des litteraux non selectionnés
        Couple c;

        while (!l.isEmpty()){
            float q = (float) Math.random();
            //calculer la vecteur de probabilité
            tn(l);
            if(q<=acs.getQ0()){
                c =Collections.max(l);
                int litteralChoise = c.getLitteral();
                interpretation.add(litteralChoise);
                l.remove(new Couple(litteralChoise));
                l.remove(new Couple(-litteralChoise));
            }else{
                soustraire(q,l);
                c =Collections.min(l);
                int litteralChoise = c.getLitteral();
                interpretation.add(litteralChoise);
                l.remove(new Couple(litteralChoise));
                l.remove(new Couple(-litteralChoise));
            }
        }
    }

    private void soustraire(float p, HashSet<Couple> l){
        Iterator<Couple> itr=l.iterator();
        while (itr.hasNext()){
            Couple c = itr.next();
            float prob = Math.abs(c.getProb() - p);
            c.setProb(prob);
            }
    }

    private void tn(HashSet<Couple> l){
        Table frequence = calculeFrequence();
        Iterator<Couple> itr = l.iterator();
        float somme =0;
        while(itr.hasNext()){
            Couple c = itr.next();
            c.setProb((float) (Math.pow(feromone.getFeromoneOf(c.getLitteral()),acs.getAlpha()) * Math.pow(frequence.getElementOf(c.getLitteral()),acs.getBeta())));
            somme = somme + c.getProb();
        }

        itr = l.iterator();

        while(itr.hasNext()){
            Couple c = itr.next();
            c.setProb(c.getProb()/somme);
           }
    }

    private Table calculeFrequence(){

        int nbVariable = acs.getChamps().getNombreVariable();
        Table frequence = new Table (nbVariable,0f);
        for (Clause clause : acs.getChamps().getClauses()) {
            boolean containAscendent = false;
            //verifier si la clause est satifiable
            for(int i: interpretation){
                if(clause.contains(i)){
                    containAscendent=true;
                    break;
                }
            }
            //si elle n'est pas satisfiable on calcule la fréquence des litteraux
            if (!containAscendent) {
                HashSet<Integer> litteraux = clause.getLitteraux();
                for (int litteral : litteraux){
                    //on incremente
                    int f = (int) frequence.getElementOf(litteral);
                   frequence.setElementOf(litteral,f+1);
                }
            }
        }
        return frequence;
    }

    private HashSet<Couple> ensembleInitial(){
        HashSet<Couple> litteralSet = new HashSet<>();
        for (int i =1;i<=acs.getChamps().getNombreVariable();i++){
            litteralSet.add(new Couple(i));
            litteralSet.add(new Couple(-i));
        }
        return litteralSet;
    }

    public HashSet<Integer> getInterpretation() {
        return (HashSet<Integer>) interpretation.clone();
    }

    public void setInterpretation(HashSet<Integer> interpretation) {
        this.interpretation = interpretation;
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public Feromone getFeromone() {
        return feromone;
    }

    public void setFeromone(Feromone feromone) {
        this.feromone = feromone;
    }

    public static ACS getAcs() {
        return acs;
    }

    public static void setAcs(ACS acs) {
        Ant.acs = acs;
    }

    @Override
    public String toString() {
        return "Ant{" +
                "interpretation="  +
                ", fitness=" + fitness +
                ", feromone=" +"\n";
    }

    @Override
    public int compareTo(Ant o) {
        return fitness - o.fitness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ant ant = (Ant) o;

        return interpretation.equals(ant.interpretation);
    }

    @Override
    public int hashCode() {
        return interpretation.hashCode();
    }
}
