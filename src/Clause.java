import java.util.HashSet;

public class Clause {
    private HashSet<Integer> litteraux;

    //setters +getters
    public Clause(HashSet<Integer> litteraux) {
        this.litteraux = litteraux;
    }

    public Clause(Clause clause) {
        this.litteraux= clause.getLitteraux();
    }

    public HashSet<Integer> getLitteraux() {
        return litteraux;
    }

    public void setLitteraux(HashSet<Integer> litteraux) {
        this.litteraux = litteraux;
    }

    @Override
    public String toString() {
        return "Clause{" +
                "litteraux=" + litteraux +
                '}'+"\n";
    }

    public boolean contains(int i) {
        return  litteraux.contains(i);
    }
}
